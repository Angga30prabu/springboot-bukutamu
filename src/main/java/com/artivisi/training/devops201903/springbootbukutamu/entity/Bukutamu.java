package com.artivisi.training.devops201903.springbootbukutamu.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.time.LocalDateTime;

@Entity @Data
public class Bukutamu {

    @Id
    private String id;
    private String email;
    private String komentar;
    private LocalDateTime waktuMengisi;
}
